#! /usr/bin/python3
#
# Description:
#   Simple scriptable tool for uploading RPN programs to RPN light string controller.
#
# Usage:
#   Upload program:
#      poke.py -t <target IP> -f <filter id> -m <modulator id> -p 'program content'
#   Reset device:
#       poke.py -t <target IP> -r
#
# Example:
#   poke.py -t 1.2.3.4 -f 0 -m 0 -p '8E 30 * 3E 30 / + 20 % O 1O 0.25O'
#
# Requires:
#   Python 3
#
#
# The MIT License (MIT)
#
# Copyright (c) 2016 K. McGinley
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import requests
import argparse
import urllib.parse
import sys

URL_PREFIX = "http://"
URL_ENDPOINT = "/poke"

has_r_arg = "--reset" in sys.argv or "-r" in sys.argv # if a -r / --reset is present we don't need args aside from IP

parser = argparse.ArgumentParser()
parser.add_argument("-t", "--target", help="target IP address or hostname", action="store", type=str, required=True)
parser.add_argument("-r", "--reset", help="reset target", action="store_true", required=False)
parser.add_argument("-f", "--filter", help="filter index", action="store", type=int, required=not has_r_arg)
parser.add_argument("-m", "--mod", help="modulator index", action="store", type=int, required=not has_r_arg)
parser.add_argument("-p", "--program", help="program string", action="store", type=str, required=not has_r_arg)
parser.add_argument("-d", "--debug", help="print additional debug info", action="store_true", required=False)

args = parser.parse_args()

if args.reset:
    print("Sending reset command...");
    params = {"r":1}
    try:
        r = requests.get(url=URL_PREFIX + args.target + URL_ENDPOINT, params=params)
    except Exception as e:
        print("Connection terminated by target.")
        if args.debug:
            print("Exception:")
            print(e)
else:
    print("Filter: %i" % args.filter)
    print("Modulator: %i" % args.mod)
    print("Program: %s" % args.program)
    print("Sending...")
    params = {"f":args.filter, "m":args.mod, "p":args.program}
    try:
        r = requests.get(url=URL_PREFIX + args.target + URL_ENDPOINT, params=params)
    except Exception as e:
        print("Connection error, request failed.")
        if args.debug:
            print("Exception:")
            print(e)
            
    if r.status_code == 200:
        print("Request suceeded: %s" % r.text)
    else:
        print("Request failed: %i" % r.status_code)