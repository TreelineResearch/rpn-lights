# RPN Christmas Lights

## Abstract

ESP32 RPN-controlled WS2801 addressable LED string driver implementation.
## Description

This firmware generates pixel sequences for a string of addressable LEDs of the WS2801
type (clock and data) using an approach vaguely similar to a stack of pixel shaders.
Programming of the filter parameters is done using a stack-based bytecode interpreter with
a semi-human-readable calculation syntax similar to Reverse Polish Notation (RPN).

A stack of four *filter* slots are provided by default, and executed in sequence for each 
pixel (LED) in the string. A filter consists of a *modulator*, which is a native code procedure
that permutes or generates a pixel value, and a *program* which is a string of bytecode that
generates parameters for the modulator. Each filter is executed on a per-pixel basis, and
several precomputed environment variables provide time-varying inputs to the RPN program.

Most math is done internally as Q16.16 fixed point, using the libfixmath library by Ben Brewer.
## WiFi Configuration

On boot, the device will attach to the WiFI access point defined in app.c by WIFI_SSID and WIFI_PASS 
and start listening on port 80 for HTTP GET requests.
## Environment (env)

- (0) ENV\_NPIXELS : Number of pixels in the string
- (1) ENV\_CPIXEL : Current pixel being processed, index into string
- (2) ENV\_TICKRATE : Number of full string updates per second
- (3) ENV\_TICK : Free running counter of ticks (string updates), rolls over at 32767
- (4) ENV\_FRAMETICK : Tick number mod tick rate
- (5) ENV\_RTCSEC : Reserved for real time clock seconds **unimplemented**
- (6) ENV\_RTCMIN : Reserved for RTC minutes **unimplemented**
- (7) ENV\_RTCHOUR : Reserved for RTC hours **unimplemented**
- (8) ENV\_POSITION : Decimal offset of current pixel being processed, from 0.0 to 1.0
## Modulators (mod)
Four modulators are provided by default:  

- (0) mod_hsl : Hue-saturation-lightness color generator
- (1) mod_gaussian : Gaussian modifier function
- (2) mod_rgb : RGB color generator
- (3) mod_flicker : Random luminance modifier function

### mod_hsl Arguments
args\[0\] = hue \[0.0 .. 359.0\]  
args\[1\] = saturation \[0.0 .. 1.0\]  
args\[2\] = luminance \[0.0 .. 1.0\]  
  
### mod_gaussian Arguments
args\[0\] = step (env\[CPIXEL\] is best here most of the time)  
args\[1\] = the height (brightness) of the gaussian hump  
args\[2\] = peak offset on the X axis (where the hump is on the string)  
args\[3\] = width of the hump (lower = narrower)  
args\[4\] = Y axis offset (controls how dark the off-hump pixels get)  
  
### mod_rgb Arguments
args\[0\] = red value \[0 .. 255\]  
args\[1\] = green value \[0 .. 255\]  
args\[2\] = blue value \[0 .. 255\]  
  
### mod_flicker Arguments
args\[0\] = value that the randomly-generated number \[0.0 .. 1.0\] is scaled (multiplied) by  
args\[1\] = offset value added to scaled random number  
  
Passing a 0 scale factor in will null out the random number, so this mod can also
be used to apply a fixed offset that is added (or subtracted, if negative) to the pixel's luminance.

# RPN Bytecode Syntax Overview
This is not a complete list of opcodes, just the ones I find most useful.  
See rpnfix.c for more details.

## Literals
Numeric literals are pushed on to the stack.  
Use spaces to seperate multiple literals, and parens to "quote" negative literals
(prevent `-` from being interpreted as subtraction).

### Example
`10 20.5 (-10)`  
Push 10, 20.5, and -10 onto the stack.  
---
## Arithmetic
`{a} {b} {arithmetic operator}`  
2-ary arithmetic operators are mostly conventional:

- `+` pop {a} and {b}, push result of {a} + {b} 
- `-` pop {a} and {b}, push result of {a} - {b} 
- `*` pop {a} and {b}, push result of {a} \* {b}  
- `/` pop {a} and {b}, push result of {a} \/ {b} 
- `%` pop {a} and {b}, push result of {a} mod {b} 

### Example
`420 69 *`  
Push 420 and 69, calculate 420 times 69, and push 28980 back on the stack.
## Conditional

`{a} {b} {comparison operator} ? {nonzero} : {zero} !`

- `?` pop value, if nonzero execute opcodes immediately following the `?` opcode until `:` is found, if zero skip until a `:` is found, and then execute from there until a `!` is found.

Comparison operators:

- `=` pop {a} and {b}, push 1 if {a} == {b}, 0 if not
- `>` pop {a} and {b}, push 1 if {a} > {b}, 0 if not
- `<` pop {a} and {b}, push 1 if {a} < {b}, 0 if not
- `l` pop {a} and {b}, push 1 if {a} <= {b}, 0 if not
- `g` pop {a} and {b}, push 1 if {a} >= {b}, 0 if not
- `N` pop {a} and {b}, push 1 if {a} != {b}, 0 if not

Logical operators:

- `&` pop {a} and {b}, push 1 if {a} && {b}, 0 if not
- `|` pop {a} and {b}, push 1 if {a} || {b}, 0 if not
- `@` pop {a}, push 1 if {a} == 0, 0 if {a} != 0
---
## Miscellaneous

Other opcodes:

- `x` push a random value between 0.0 and 1.0 onto to the stack
- maybe a placeholder here will make the markdown render correctly
---
## Environment
Environment variables are read only. Push a literal index, `E` pops it off,
accesses env\[index\], and pushes the value of the variable back
onto the stack.
### Example
`0 E`
Push environment index 0 (ENV\_NPIXELS), `E` pops it, reads the value of env\[0\] and pushes it on the stack.
For this case, the value pushed back will be 50, set at compile time by the NPIXELS macro define in app.c.
---
## Scratch
The scratch buffer is a read-write list that can be used to store literals and is persistent between
runs of the program and accessible by all filters. Consider this a small amount of RAM that's shared
between the programs and filters.

Two opcodes interact with the scratch buffer, `R` (read) and `W` (write).

`R` works just like `E`, popping off an index into the buffer and pushing the value at that index back
on to the stack.

`W` pops the index and literal, and stores the literal at scratch\[index\].

### Example
`0 R`
Read value at scratch\[0\] and push it onto the stack.

`420 0 W`
Push literal (420) onto the stack, push index (0), `W` pops both and sets scratch\[0\] = 420.
---
## Output
Outputs are how values calculated in RPN programs are passed into native-code modulator functions.
One opcode `O` is provided, which pops the value on the top of the stack and inserts it, auto-incrementing,
into the argument vector passed to the modulator function associated with the executing program.
`O` can almost be considered a "divider" or "return" opcode that seperates subprograms associated with each argument.
### Example
`4.20 6.9 * O 4200 10 / O 0.1O`  
Push 4.20 and 6.9, multiply, and output the result as args\[0],
then divide 4200 by 10 and output that result as args\[1],
then output 0.1 literal as args\[3].  
(Spaces are generally optional between command opcodes, but can make programs slightly more readable.)
---
# Putting it all together / example filter stacks
A filter is a modulator with an associated program, and there are 4 of them per pixel per render pass
executed in sequence from 0 to 3. This allows stacking multiple effects.
## Static HSL Spectrum
*Rotate the hue per-pixel by the position of the current pixel being processed to generate a static HSL rainbow.*

Filter:  0  
Modulator: 0 (mod_hsl)  
Program: `8E 359 * O 1.0 O 0.5 O`  
### Explanation
Multiply ENV\_POSITION, which is 0.0 at the beginning of the string and approaches 1.0 at the end,
by 359 and output it as args\[0\] (hue), then output 1.0 as args\[1\] (saturation), and finally
output 0.5 as args\[2\] (luminance).
## HSL Chase
*Similar to above, but add in the current tick mod 359 to cause the rainbow to move across the string over time.*
Filter: 0  
Modulator: 0  
Program: `8E 359 * 3E 5 / + 359 % O 1O 0.5O`  
### Explanation
- Multiply ENV\_POSITION (env\[8\]; 8E) by 359, leaving result on the stack
- Divide ENV\_TICK (env\[3\]; 3E) by 5, leaving result on the stack (divide to slow the chase, multiply to speed it up)
- Add the above together, leaving result on the stack
- Apply modulo 359 to the above value, leaving the result on the stack
- Output it as args\[0\] (hue)
- Output 1 as args\[1\] (saturation)
- Output 0.5 as args\[2\] (luminance)
## HSL Chase with Gaussian Sweep
*Same HSL program as above, but we'll put a moving gaussian peak in filter slot 1 to make a brighter spot sweep across the string.*  
Filter: 0  
Modulator: 0  
Program: `8E 359 * 3E 5 / + 359 % O 1O 0.5O`  
*explanation same as above*  
Filter: 1  
Modulator: 1 (mod_gaussian)  
Program: `1EO 1O 3E 10 / 0E % O 0.5O 0.05O`  
### Explanation
- Output ENV\_CPIXEL (env\[1\]; 1E) as args\[0\] (step) to mod_gaussian
- Output 1 as args\[1\] the gaussian peak height
- Divide ENV\_TICK (env\[3\]; 3E) by 10, and push its value modulo ENV\_NPIXELS (env\[0\]; 0E) onto the stack
- Output above result as args\[2\], the gaussian peak X offset (position of the peak on the string)
- Output 0.5 as args\[3\], the gaussian peak width (fairly narrow)
- Output 0.05 as args\[4\], the gaussian peak Y offset (brightness offset)
## HSL Chase with Gaussian Sweep and Flicker
*Same as above but add a small amount of random flicker*  
Filter: 0  
Modulator: 0  
Program: `8E 359 * 3E 5 / + 359 % O 1O 0.5O`   
*explanation same as above*  
Filter: 1  
Modulator: 1  
Program: `1EO 1O 3E 10 / 0E % O 0.5O 0.05O`  
*explanation same as above*  
Filter: 2  
Modulator: 3 (mod_flicker)  
Program: `0.05O 0O`  
### Explanation
- Output 0.05 as args\[0\] (random value scale factor) to mod_flicker
- Output 0 as args\[1\] (luminance offset)
---
# poke.py
A python script to upload parameters to a controller, one filter at a time.  
Requires Python 3. On Windows invoke with `python3 poke.py` since the shebang won't work.

```
usage: poke.py [-h] -t TARGET [-r] -f FILTER -m MOD -p PROGRAM [-d]

optional arguments:
  -h, --help            show this help message and exit
  -t TARGET, --target TARGET
                        target IP address or hostname
  -r, --reset           reset target
  -f FILTER, --filter FILTER
                        filter index
  -m MOD, --mod MOD     modulator index
  -p PROGRAM, --program PROGRAM
                        program string
  -d, --debug           print additional debug info
```
## Example Usage

### Upload HSL chase with sweep and flicker shown above
From a command line with Python 3 installed:  
`$ ./poke.py -t <device IP> -f 0 -m 0 -p '8E 359 * 3E 5 / + 359 % O 1O 0.5O'`  
`$ ./poke.py -t <device IP> -f 1 -m 1 -p '1EO 1O 3E 10 / 0E % O 0.5O 0.05O'`  
`$ ./poke.py -t <device IP> -f 2 -m 3 -p '0.05O 0O'`  

### Reboot Device
`$ ./poke.py -t <device IP> -r`