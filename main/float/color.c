#include <stdint.h>
#include <math.h>
#include "color.h"

hsl_t rgb_to_hsl(pixel_t input) {
	hsl_t temp;
	float rfrac = input.r / 255.0F;
	float gfrac = input.g / 255.0F;
	float bfrac = input.b / 255.0F;
	
	float min = fminf(fminf(rfrac, gfrac), bfrac);
	float max = fmaxf(fmaxf(rfrac, gfrac), bfrac);	
	temp.l = (min + max) / 2.0F;
	if(min == max) { temp.s = 0.0F; } 
	else {
		if(temp.l < 0.5F) {
			temp.s = (max - min) / (max + min);
		}
		else {
			temp.s = (max - min) / (2.0F - max - min);
		}
	}
	if(rfrac > gfrac && rfrac > bfrac) {
		temp.h = (gfrac - bfrac) / (max - min);
	}
	else if(gfrac > rfrac && gfrac > bfrac) {
		temp.h = 2.0F + (bfrac - rfrac) / (max - min);
	}
	else if(bfrac > rfrac && bfrac > rfrac) {
		temp.h = 4.0 + (rfrac - gfrac) / (max - min);
	}
	
	temp.h *= 60.0F;
	if(temp.h < 0)
		temp.h += 360.0F;
	
	return temp;
}

pixel_t hsl_to_rgb(float h, float s, float l) {
	pixel_t outpixel;
	if((uint8_t)(s * 100.0F) == 0) {
		uint8_t greyval = (uint8_t)(l * 255.0F);
		outpixel.r = greyval;
		outpixel.g = greyval;
		outpixel.b = greyval;
		return outpixel;
	}
	else {
		float tr = 0.0F, tg = 0.0F, tb = 0.0F;
		float c = (1.0F - fabsf((2.0F * l) - 1.0F)) * s;
		float hp = h / 60.0F;
		float x = c * (1.0F - fabsf(fmodf(hp, 2.0F) - 1.0F));
		
		if(hp >= 0.0F && hp < 1.0F) {
			tr = c;
			tg = x;
			tb = 0.0F;
		}
		else if(hp >= 1.0F && hp < 2.0F) {
			tr = x;
			tg = c;
			tb = 0.0F;				
		}
		else if(hp >= 2.0F && hp < 3.0F) {
			tr = 0.0F;
			tg = c;
			tb = x;				
		}
		else if(hp >= 3.0F && hp < 4.0F) {
			tr = 0.0F;
			tg = x;
			tb = c;				
		}
		else if(hp >= 4.0F && hp < 5.0F) {
			tr = x;
			tg = 0.0F;
			tb = c;				
		}
		else if(hp >= 5.0F && hp < 6.0F) {
			tr = c;
			tg = 0.0F;
			tb = x;				
		}
		else {
			tr = 0.0F;
			tg = 0.0F;
			tb = 0.0F;
		}
		
		float m = l-c/2.0F;
		
		outpixel.r = (uint8_t)((tr + m) * 255.0F);
		outpixel.g = (uint8_t)((tg + m) * 255.0F);
		outpixel.b = (uint8_t)((tb + m) * 255.0F);
	}
	return outpixel;
}

pixel_t pixel_mul_scalar(pixel_t a, float scalar) {
	a.r = CLAMP((uint8_t)((float)a.r * scalar), 0, 255);
	a.g = CLAMP((uint8_t)((float)a.g * scalar), 0, 255);
	a.b = CLAMP((uint8_t)((float)a.b * scalar), 0, 255);
	
	return a;
}

pixel_t pixel_add_scalar(pixel_t a, float scalar) {
	a.r = CLAMP((uint8_t)((float)a.r + scalar), 0, 255);
	a.g = CLAMP((uint8_t)((float)a.g + scalar), 0, 255);
	a.b = CLAMP((uint8_t)((float)a.b + scalar), 0, 255);
	
	return a;
}