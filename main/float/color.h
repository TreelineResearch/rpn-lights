#ifndef __COLOR_H__
#define __COLOR_H__

#define CLAMP(x, low, high)  (((x) > (high)) ? (high) : (((x) < (low)) ? (low) : (x)))

typedef uint32_t pixel_size_t;
typedef union pixel {
    pixel_size_t i;
    struct {
        pixel_size_t b : 8;
		pixel_size_t g : 8;
        pixel_size_t r : 8;
		pixel_size_t a : 8;		
    };
} pixel_t;

typedef struct hsl {
    float h;
	float s;
    float l;		
} hsl_t;

hsl_t rgb_to_hsl(pixel_t input);
pixel_t hsl_to_rgb(float h, float s, float l);
pixel_t pixel_mul_scalar(pixel_t a, float scalar);
pixel_t pixel_add_scalar(pixel_t a, float scalar);

#endif