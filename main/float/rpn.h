#ifndef __RPN_H__
#define __RPN_H__
#define PROG_SIZE 128
#define PROG_OUTBUF_SIZE 8
#define RPN_WBUFLEN 8
#define RPN_MAX_ITER 10
#define RPN_STACK_MAX 8
#define RPN_STACK_EMPTY -1
#define RPN_STACK_OVERFLOW -99

#define RPN_READ_FLAG 1
#define RPN_POINT_FLAG 2

float rpn_push(float val);
float rpn_pop(void);

float rpn_evaluate(char *program, float *env, float *scratch, float *out);

#endif