/*
Copyright (C) 2019 K. McGinley

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <stdint.h>
#include <math.h>
#include "colorfix.h"

const fix16_t FIX_0_5 = 0x8000;
const fix16_t FIX_2 = F16(2);
const fix16_t FIX_3 = F16(3);
const fix16_t FIX_4 = F16(4);
const fix16_t FIX_5 = F16(5);
const fix16_t FIX_6 = F16(6);
const fix16_t FIX_60 = F16(60);
const fix16_t FIX_100 = F16(100);
const fix16_t FIX_255 = F16(255);
const fix16_t FIX_360 = F16(360);

hsl_t rgb_to_hsl(pixel_t input) {
	hsl_t temp;

	fix16_t rfrac = fix16_div(fix16_from_int(input.r), FIX_255);
	fix16_t gfrac = fix16_div(fix16_from_int(input.g), FIX_255);
	fix16_t bfrac = fix16_div(fix16_from_int(input.b), FIX_255);
	
	fix16_t min = fix16_min(fix16_min(rfrac, gfrac), bfrac);
	fix16_t max = fix16_max(fix16_max(rfrac, gfrac), bfrac);	

	fix16_t maxsubmin = fix16_sub(max, min);
	fix16_t maxaddmin = fix16_add(max, min);
	
	temp.l = fix16_div(maxaddmin, FIX_2);


	if(min == max) { temp.s = 0; } 
	else {
		if(temp.l < FIX_0_5) {
			temp.s = fix16_div(maxsubmin, maxaddmin);
		}
		else {
			temp.s = fix16_div(maxsubmin, fix16_sub(fix16_sub(FIX_2, max), min));
		}
	}
	if(rfrac >= gfrac && rfrac >= bfrac) {
		temp.h = fix16_div(fix16_sub(gfrac, bfrac), maxsubmin);
	}
	else if(gfrac >= rfrac && gfrac >= bfrac) {
		temp.h = fix16_add(FIX_2, fix16_div(fix16_sub(bfrac, rfrac), maxsubmin));
	}
	else if(bfrac >= rfrac && bfrac >= gfrac) {
		temp.h = fix16_add(FIX_4, fix16_div(fix16_sub(rfrac, gfrac), maxsubmin));
	}
	
	temp.h = fix16_mul(temp.h, FIX_60);
	if(temp.h < 0) {
		temp.h = fix16_add(temp.h, FIX_360);
	}
	
	return temp;
}

pixel_t hsl_to_rgb(fix16_t h, fix16_t s, fix16_t l) {
	pixel_t outpixel;
	if(s == 0) {
		uint8_t greyval = (uint8_t)fix16_to_int(fix16_mul(l, FIX_255));
		outpixel.r = greyval;
		outpixel.g = greyval;
		outpixel.b = greyval;
		return outpixel;
	}
	else {
		fix16_t tr = 0, tg = 0, tb = 0;

		fix16_t c2l = fix16_mul(FIX_2, l);
		fix16_t ca = fix16_sub(fix16_one, fix16_abs(fix16_sub(c2l, fix16_one)));
		fix16_t c = fix16_mul(ca, s);

		fix16_t hp = fix16_div(h, FIX_60);
		fix16_t hp_mod2 = fix16_mod(hp, FIX_2);
		
		fix16_t xa = fix16_sub(fix16_one, fix16_abs(fix16_sub(hp_mod2, fix16_one)));
		fix16_t x  = fix16_mul(c, xa);
		
		if(hp >= 0 && hp < fix16_one) {
			tr = c;
			tg = x;
			tb = 0;
		}
		else if(hp >= fix16_one && hp < FIX_2) {
			tr = x;
			tg = c;
			tb = 0;				
		}
		else if(hp >= FIX_2 && hp < FIX_3) {
			tr = 0;
			tg = c;
			tb = x;				
		}
		else if(hp >= FIX_3 && hp < FIX_4) {
			tr = 0;
			tg = x;
			tb = c;				
		}
		else if(hp >= FIX_4 && hp < FIX_5) {
			tr = x;
			tg = 0;
			tb = c;				
		}
		else if(hp >= FIX_5 && hp < FIX_6) {
			tr = c;
			tg = 0;
			tb = x;				
		}
		else {
			tr = 0;
			tg = 0;
			tb = 0;
		}
		
		fix16_t m = fix16_sub(l, fix16_div(c, FIX_2));
		outpixel.r = (uint8_t)fix16_to_int(fix16_mul(fix16_add(tr, m), FIX_255));
		outpixel.g = (uint8_t)fix16_to_int(fix16_mul(fix16_add(tg, m), FIX_255));
		outpixel.b = (uint8_t)fix16_to_int(fix16_mul(fix16_add(tb, m), FIX_255));
	}
	return outpixel;
}

/*
pixel_t pixel_mul_scalar(pixel_t a, float scalar) {
	a.r = CLAMP((uint8_t)((float)a.r * scalar), 0, 255);
	a.g = CLAMP((uint8_t)((float)a.g * scalar), 0, 255);
	a.b = CLAMP((uint8_t)((float)a.b * scalar), 0, 255);
	
	return a;
}

pixel_t pixel_add_scalar(pixel_t a, float scalar) {
	a.r = CLAMP((uint8_t)((float)a.r + scalar), 0, 255);
	a.g = CLAMP((uint8_t)((float)a.g + scalar), 0, 255);
	a.b = CLAMP((uint8_t)((float)a.b + scalar), 0, 255);
	
	return a;
}
*/