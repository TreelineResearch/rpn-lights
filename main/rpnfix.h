/*
Copyright (C) 2019 K. McGinley

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef __RPN_H__
#define __RPN_H__

#include "fixmath.h"

#define PROG_SIZE 128
#define PROG_OUTBUF_SIZE 8
#define RPN_WBUFLEN 8
#define RPN_MAX_ITER 10
#define RPN_STACK_MAX 8
#define RPN_STACK_EMPTY -1
#define RPN_STACK_OVERFLOW -99

#define RPN_READ_FLAG 1
#define RPN_POINT_FLAG 2

typedef union {
    fix16_t f;
    struct {
        int16_t frac;
        uint16_t i;
    };
} fixint_t;

int16_t rpn_push(fixint_t val);
fixint_t rpn_pop(void);

fixint_t rpn_evaluate(char *program, fix16_t *env, fix16_t *scratch, fix16_t *out);

#endif
