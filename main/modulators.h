#ifndef __MODULATORS_H__
#define __MODULATORS_H__
#include <stdlib.h>
#include "fixmath.h"
#include "colorfix.h"

pixel_t pixel_muls(pixel_t pixel, fix16_t m);
pixel_t mod_hsl(pixel_t input, fix16_t *env, fix16_t *args);
pixel_t mod_flicker(pixel_t input, fix16_t *env, fix16_t *args);
pixel_t mod_gaussian(pixel_t input, fix16_t *env, fix16_t *args);
pixel_t mod_rgb(pixel_t input, fix16_t *env, fix16_t *args);

#endif