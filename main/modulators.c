/*
Copyright (C) 2019 K. McGinley

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "modulators.h"

/*
	Multiply pixel RGB values by input constant, clamp to 0 .. 255
*/
pixel_t pixel_muls(pixel_t pixel, fix16_t m) {
	pixel.r = fix16_to_int(fix16_clamp(fix16_smul(fix16_from_int(pixel.r), m), 0, F16(255)));
	pixel.g = fix16_to_int(fix16_clamp(fix16_smul(fix16_from_int(pixel.g), m), 0, F16(255)));
	pixel.b = fix16_to_int(fix16_clamp(fix16_smul(fix16_from_int(pixel.b), m), 0, F16(255)));
	
	return pixel;
}

/*
	Replace pixel with a new value generated from HSL by fix16 parameters in args:
		args[0] = hue (0 .. 359.0) 
		args[1] = saturation (0 .. 1.0)
		args[2] = luminance (0 .. 1.0)
*/
pixel_t mod_hsl(pixel_t input, fix16_t *env, fix16_t *args) {
	float hue = fix16_from_int(fix16_to_int(args[0]) % 359);
	float sat = fix16_clamp(args[1], 0, fix16_one);
	float lum = fix16_clamp(args[2], 0, fix16_one);

	return hsl_to_rgb(hue, sat, lum);
}
/*
	Modify input pixel luminance by a random value from 0 to 1, scaled by fix16 args[0]
	and then added to constant args[1].
	Scaling and constant arguments can be negative or positive, passing a 0 scale will
	null out the random term and only the constant will be applied.
	
	Just permuting the RGB values by a constant is much faster but does not 
	look as good because the hue/sat changes depending on the proportion of the
	modifier value to the subpixel value, so we go the more computationally
	expensive way of RGB -> HSL -> L + (rand * args[0]) + args[1] -> RGB.
*/
pixel_t mod_flicker(pixel_t input, fix16_t *env, fix16_t *args) {
	fix16_t rfix = fix16_from_float((float)rand() / (float)RAND_MAX); // fixme (literally; this should probably be done in fix16 space)
	hsl_t hslpix = rgb_to_hsl(input);
	
	hslpix.l = fix16_sadd(hslpix.l, fix16_mul(rfix, args[0]));
	hslpix.l = fix16_sadd(hslpix.l, args[1]);
	input = hsl_to_rgb(hslpix.h, hslpix.s, hslpix.l);
	return input;
}
/*
	Apply parametric Gaussian function (that humpy curve thing).
		args[0] step (environment[CPIXEL] most of the time)
		args[1] the height of the hump
		args[2] peak offset on the X axis (where the hump is on the string)
		args[3] width of the hump, lower = narrower
		args[4] Y axis offset
		
	This should probably be done in luminance space too but it works well enough in RGB.
*/
pixel_t mod_gaussian(pixel_t input, fix16_t *env, fix16_t *args) {
	fix16_t gs = args[0]; // a[0] step
	fix16_t ga = args[1]; // a[1] the height of the peak
	fix16_t gb = args[2]; // a[2] peak offset on the X axis
	fix16_t gc = args[3]; // a[3] width of the peak, lower = narrower
	fix16_t gd = args[4]; // a[4] Y axis offset

	// the gaussian function:
	// ga * exp( -( ((x-gb)^2) / 2 * gc^2 ) )+gd
	// in this case, x = gs
	fix16_t t = fix16_sdiv(
		                  fix16_sq(fix16_ssub(gs, gb)), 
		                  fix16_smul(F16(2), fix16_sq(gc)) 
		                 );
	fix16_t g = fix16_sadd(fix16_smul(ga, fix16_exp(fix16_ssub(0, t))), gd);
	return pixel_muls(input, g);
}

/*
	Replace pixel with a new value generated from RGB by fix16 parameters in args:
		args[0]: red channel
		args[1]: green channel
		args[2]: blue channel
*/
pixel_t mod_rgb(pixel_t input, fix16_t *env, fix16_t *args) {
	input.r = fix16_to_int(fix16_clamp(args[0], 0, 255));
	input.g = fix16_to_int(fix16_clamp(args[1], 0, 255));
	input.b = fix16_to_int(fix16_clamp(args[2], 0, 255));
	
	return input;
}