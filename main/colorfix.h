/*
Copyright (C) 2019 K. McGinley

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef __COLOR_H__
#define __COLOR_H__

#include "fixmath.h"

#define CLAMP(x, low, high)  (((x) > (high)) ? (high) : (((x) < (low)) ? (low) : (x)))

typedef uint32_t pixel_size_t;
typedef union pixel {
    pixel_size_t i;
    struct {
        pixel_size_t b : 8;
		pixel_size_t g : 8;
        pixel_size_t r : 8;
		pixel_size_t a : 8;		
    };
} pixel_t;

typedef struct hsl {
    fix16_t h;
	fix16_t s;
    fix16_t l;		
} hsl_t;

hsl_t rgb_to_hsl(pixel_t input);
pixel_t hsl_to_rgb(fix16_t h, fix16_t s, fix16_t l);
//pixel_t pixel_mul_scalar(pixel_t a, fix16_t scalar);
//pixel_t pixel_add_scalar(pixel_t a, fix16_t scalar);

#endif