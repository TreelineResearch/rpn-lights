/*
Copyright (C) 2019 K. McGinley

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "esp32/rom/ets_sys.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_http_server.h"

#include "lwip/err.h"
#include "lwip/sys.h"


#include "fixmath.h"
#include "rpnfix.h"
#include "colorfix.h"
#include "modulators.h"

#define CLK_GPIO 			19
#define DATA_GPIO 			18
#define CLK_GPIO_REG 		BIT19
#define DATA_GPIO_REG 		BIT18

#define WIFI_SSID			"<YOUR SSID HERE>"
#define WIFI_PASS			"<YOUR PASSWORD HERE>"
#define WIFI_MAX_RETRY		8

#define NPIXELS				50
#define ENVIRONMENT_SIZE	16
#define SCRATCH_SIZE		16
#define RESULT_SIZE			16
#define NPROGS				8
#define NMODULATORS			4
#define PROG_BUF_SIZE		256
#define FILTER_LIST_SIZE	4
#define TICKRATE			30
#define FRAME_TIME			((uint16_t)((1.0F/TICKRATE) * 1000))

#define ENV_NPIXELS			0
#define ENV_CPIXEL			1
#define ENV_TICKRATE		2
#define ENV_TICK			3
#define ENV_FRAMETICK		4
#define ENV_RTCSEC			5
#define ENV_RTCMIN			6
#define ENV_RTCHOUR			7
#define ENV_POSITION		8

static EventGroupHandle_t s_wifi_event_group;

uint16_t sys_ticks = 0;
pixel_t pixelbuf[NPIXELS];
fix16_t environment[ENVIRONMENT_SIZE];
fix16_t scratch[SCRATCH_SIZE];
fix16_t result[RESULT_SIZE];

const int WIFI_CONNECTED_BIT = BIT0;
static const char *TAG = "TLRRPNXMAS";
static int s_retry_num = 0;

// A filter is defined by an RPN program string and a pointer to the 
// modulator function it generates parameters for.
typedef struct filter {
	char program[PROG_BUF_SIZE];
	pixel_t (*mod)(pixel_t, fix16_t *, fix16_t *);
} filter_t;

filter_t filter_list[FILTER_LIST_SIZE];

// Array of modulator function pointers
pixel_t (*modptr_list[NMODULATORS])(pixel_t, fix16_t *, fix16_t *) = {
	mod_hsl,
	mod_gaussian,
	mod_rgb,
	mod_flicker
};

void log_filters(filter_t *filters) {
	for(uint8_t i = 0; i < FILTER_LIST_SIZE; i++) {
		ESP_LOGI(TAG, "filter[%i] = {modptr: 0x%X program: `%s`}", i, (uint32_t)filters[i].mod, filters[i].program);
	}
}

static void event_handler(void* arg, esp_event_base_t event_base, 
                                int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
        esp_wifi_connect();
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
        if (s_retry_num < WIFI_MAX_RETRY) {
            esp_wifi_connect();
            xEventGroupClearBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
            s_retry_num++;
            ESP_LOGI(TAG, "Retrying connection, attempt %i", s_retry_num);
        }
        ESP_LOGI(TAG,"Failed to connect to SSID `%s`", WIFI_SSID);
    } else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(TAG, "Got IP " IPSTR, IP2STR(&event->ip_info.ip));
        s_retry_num = 0;
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    }
}

void wifi_init_sta(void)
{
    s_wifi_event_group = xEventGroupCreate();

    esp_netif_init();

    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif_create_default_wifi_sta();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));


    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler, NULL));

    wifi_config_t wifi_config = {
        .sta = {
            .ssid = WIFI_SSID,
            .password = WIFI_PASS
        },
    };
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start() );
    ESP_LOGI(TAG, "Connecting to AP SSID %s", WIFI_SSID);
}


/*
	Just sets up the hardware ports as GPIO outputs.
*/
void init_port(void) {
    gpio_pad_select_gpio(CLK_GPIO);
    gpio_pad_select_gpio(DATA_GPIO);	
    gpio_set_direction(CLK_GPIO, GPIO_MODE_OUTPUT);
	gpio_set_direction(DATA_GPIO, GPIO_MODE_OUTPUT);	
}

/*
	Just blanks out the buffers and initializes the environment.
*/
void init_buffers() {
	memset(filter_list, 0, sizeof(filter_t) * FILTER_LIST_SIZE);
	memset(environment, 0, sizeof(fix16_t) * ENVIRONMENT_SIZE);
	memset(scratch, 0, sizeof(fix16_t) * SCRATCH_SIZE);
	memset(result, 0, sizeof(fix16_t) * RESULT_SIZE);	

	for(uint8_t i = 0; i < NPIXELS; i++) {
		pixelbuf[i].i = 0;
		pixelbuf[i].a = 255; // Nothing currently uses alpha but set it to full anyway
	}
	
	// Reset tick counter
	sys_ticks = 0;
	
	// Set environment parameters that won't change at runtime
	environment[ENV_NPIXELS] = fix16_from_int(NPIXELS);
	environment[ENV_TICKRATE] = fix16_from_int(TICKRATE);	
}

/*
	Transmit start condition (data=0, clk=0 for > 500us) to the string.
*/
void start_send() {
	portMUX_TYPE mutex = portMUX_INITIALIZER_UNLOCKED;		
	portENTER_CRITICAL(&mutex);
	REG_WRITE(GPIO_OUT_W1TS_REG, DATA_GPIO_REG);
	REG_WRITE(GPIO_OUT_W1TS_REG, CLK_GPIO_REG);	 // Output is inverted by the driver
	ets_delay_us(550);
	portEXIT_CRITICAL(&mutex);
}

/* 
	Transmit a 24 bit pixel to the WS2801 string. 
	Note that a WS2801 uses a 2-wire clock and data scheme, not the 1-wire scheme that WS2811 uses.
*/
void send_pixel(pixel_t buf) {
	/* 
		Note 1: 
			Not sure why, but if the delay is inserted after the conditional only
			(the "note 1b" position), clock pulses occasionally get dropped and the
			string glitches, even with interrupts [hypothetically] turned off. 
			Putting a delay after each conditional ("note 1a" positions) fixes this.

			May have something to do with the instruction caching ESP32 does, 
			or perhaps something running on the other core.
			
			In the words of Bill O, "can't explain it" and in the words of Bill J, "don't ask me why".			
	*/
	portMUX_TYPE mutex = portMUX_INITIALIZER_UNLOCKED;		
	portENTER_CRITICAL(&mutex);
	for(int8_t i = 23; i >= 0; i--) {		
		if((buf.i & (1 << i)) == 0) {
			REG_WRITE(GPIO_OUT_W1TS_REG, DATA_GPIO_REG);	
			ets_delay_us(1); // note 1a
		}
		else {
			REG_WRITE(GPIO_OUT_W1TC_REG, DATA_GPIO_REG);
			ets_delay_us(1); // note 1a
		}
		ets_delay_us(1); //note 1b
		REG_WRITE(GPIO_OUT_W1TC_REG, CLK_GPIO_REG);
		ets_delay_us(2);	
		REG_WRITE(GPIO_OUT_W1TS_REG, CLK_GPIO_REG);					
	}
	portEXIT_CRITICAL(&mutex);
}

/*
	For each pixel in the pixel buffer, run rpn_program and pass its output
	to modulator function pointed to by modfn, leaving the generated/modified result in its place.
*/
void apply_mod(pixel_t *pixbuf, pixel_t (*modfn)(pixel_t, fix16_t *, fix16_t *), char *rpn_program, fix16_t *env, fix16_t *scratch, fix16_t *result) {
	for(uint8_t i = 0; i < NPIXELS; i++) {
	
		// Purge result/argument buffer
		memset(result, 0, sizeof(fix16_t) * RESULT_SIZE);	
		
		// Calculate environment parameters that change per-pixel
		env[ENV_CPIXEL] = fix16_from_int(i);
		env[ENV_POSITION] = fix16_div(env[ENV_CPIXEL], env[ENV_NPIXELS]); // first pixel = 0.0, last pixel = ~1.0, middle pixel = ~0.5
		env[ENV_FRAMETICK] = fix16_mod(fix16_from_int(sys_ticks), fix16_from_int(TICKRATE)); // tick mod framerate
		
		// Run the RPN program, passing in the environment buffer, scratch buffer, and result buffer.
		// Environment parameters are read-only from RPN space.
		// Scratch parameters are read/write and persistent.
		// Results (arguments to the modulator) are write-only and sequentially outputted by the O bytecode.
		// See rpn.c for more details on how this works.
		rpn_evaluate(rpn_program, env, scratch, result);
		
		// Run modfn and store the value in the pixel buffer
		pixbuf[i] = (*modfn)(pixbuf[i], env, result);
	}
}

/*
	Paint the string.
	This function is called periodically at the rate set by FRAME_TIME.
	The environment parameters are updated, then the filter stack is run through in order, 
	modifying pixelbuf, which is then transmitted to the string. 
	
	Tick timer (1 tick = 1 frame) is also updated here.
	
	If the string doesn't fully paint (tail end doesn't update or is updated sporadically), 
	the modulator functions are taking too long to complete and the next update is starting
	before the transmission is finished. The timer can be stopped at the beginning and started
	at the end, guaranteeing a full paint but not a fixed frame rate. 
	In this case, the FRAME_TIME would act more like a framerate limiter than a time guarantee.
	
	Each approach has its upsides and downsides, but we'll go with constant time frames since
	the fixmath modulators go pretty fast.
*/
void frame_callback(TimerHandle_t th) {
	// Calculate environment parameters that change per-frame
	environment[ENV_TICK] = fix16_from_int(sys_ticks & 0x7FFF);
	
	// Apply all the filters that have non-null modulator function pointers
	for(uint8_t fi = 0; fi < FILTER_LIST_SIZE; fi++) {
		if(filter_list[fi].mod == NULL) continue;
		apply_mod(pixelbuf, filter_list[fi].mod, filter_list[fi].program, environment, scratch, result);
	}
	
	start_send();
	for(uint8_t i = 0; i < 50; i++) {
		send_pixel(pixelbuf[i]);
	}
	sys_ticks++;
}
/*
	URL/percent decode string in input, leaving it in output.
	
	It's shitty because it's both not validated and there are lots of error
	conditions it will happily ignore!
*/
uint16_t shitty_urldecode(char* input, char* output) {
	uint16_t inlen = strlen(input);
	uint16_t inpos = 0;
	uint16_t outpos = 0;
	char code[3];	
	while(inpos < inlen) {
		if(input[inpos] == '+') {
			output[outpos] = ' ';
		}
		else if (input[inpos] == '%' && (inpos + 2) < inlen) {
			uint8_t codepos = 0;
			code[codepos++] = input[++inpos];
			code[codepos++] = input[++inpos];
			code[codepos] = '\0';
			char c = strtol(code, NULL, 16);
			output[outpos] = c;
		}
		else if (input[inpos] == '%') {
			continue;
		}
		else {
			output[outpos] = input[inpos];
		}
		inpos++;
		outpos++;
	}
	output[outpos] = '\0';
	return outpos;
}

esp_err_t get_handler(httpd_req_t *req)
{
	bool error_flag = false;
	char error_msg[128];
	int32_t reset = 0;
	int32_t clear = 0;
	int32_t filter_id = -2;
	int32_t modulator_id = -2;
	char program_data[PROG_BUF_SIZE+1];
	char program_data_decoded[PROG_BUF_SIZE+1];
	uint16_t prog_len = -1;
	
    const char resp_ok[] = "Ok";
	
	char *buf;
	size_t buf_len = httpd_req_get_url_query_len(req) + 1;
    if (buf_len > 1) {
        buf = malloc(buf_len);
        if (httpd_req_get_url_query_str(req, buf, buf_len) == ESP_OK) {
            char param[256];
            if(httpd_query_key_value(buf, "f", param, sizeof(param)) == ESP_OK) {
                ESP_LOGI(TAG, "Found filter=`%s`", param);
				if(strlen(param) > 0) {
					filter_id = strtol(param, NULL, 10);
				}
            }
            if(httpd_query_key_value(buf, "m", param, sizeof(param)) == ESP_OK) {
                ESP_LOGI(TAG, "Found modulator=`%s`", param);
				if(strlen(param)) {
					modulator_id = strtol(param, NULL, 10);
				}
            }
            if(httpd_query_key_value(buf, "p", param, sizeof(param)) == ESP_OK) {
                ESP_LOGI(TAG, "Found program=`%s`", param);
				prog_len = strlen(param);
				strlcpy(program_data, param, (prog_len + 1) < PROG_BUF_SIZE ? (prog_len + 1) : PROG_BUF_SIZE);
            }
            if(httpd_query_key_value(buf, "r", param, sizeof(param)) == ESP_OK) {
                ESP_LOGI(TAG, "Found reset=`%s`", param);
				if(strlen(param)) {
					reset = strtol(param, NULL, 10);
				}
            }
            if(httpd_query_key_value(buf, "c", param, sizeof(param)) == ESP_OK) {
                ESP_LOGI(TAG, "Found clear=`%s`", param);
				if(strlen(param)) {
					clear = strtol(param, NULL, 10);
				}
            }			
        }
        free(buf);
    }
	
	if(filter_id >= 0) {
		if(filter_id < FILTER_LIST_SIZE) {
			if(modulator_id >= 0 && modulator_id < NMODULATORS) {
				filter_list[filter_id].mod = modptr_list[modulator_id];
				prog_len = strlen(program_data);
				if(prog_len >= 1) {
					shitty_urldecode(program_data, program_data_decoded);
					ESP_LOGI(TAG, "decoded: %s", program_data_decoded);
					strlcpy(filter_list[filter_id].program, program_data_decoded, (prog_len + 1));
				}
				else {
					filter_list[filter_id].program[0] = '\0';
				}
			}
			else if(modulator_id == -1) {
				filter_list[filter_id].mod = NULL;
			}
			else {
				error_flag = true;
				char msg[] = "Error: modulator index out of range";
				strlcpy(error_msg, msg, strlen(msg)); 
			}
		}
		else {
			error_flag = true;
			char msg[] = "Error: filter index out of range";
			strlcpy(error_msg, msg, strlen(msg)); 
		}
	}
	else if(reset > 0) {
		esp_restart();
	}
	else if(clear > 0) {
		init_buffers();
	}
	else {
		error_flag = true;
		char msg[] = "Error: bad argument";
		strlcpy(error_msg, msg, strlen(msg)); 		
	}
	
	ESP_LOGI(TAG, "filter: %i, modulator: %i, data: `%s`", filter_id, modulator_id, program_data);
	log_filters(filter_list);
	if(error_flag) {
		httpd_resp_send(req, error_msg, strlen(error_msg));
	}
	else {
		httpd_resp_send(req, resp_ok, strlen(resp_ok));				
	}
    return ESP_OK;
}

httpd_uri_t uri_get = {
    .uri      = "/poke",
    .method   = HTTP_GET,
    .handler  = get_handler,
    .user_ctx = NULL
};

httpd_handle_t start_webserver(void)
{
    /* Generate default configuration */
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();

    /* Empty handle to esp_http_server */
    httpd_handle_t server = NULL;

    /* Start the httpd server */
    if (httpd_start(&server, &config) == ESP_OK) {
        /* Register URI handlers */
        httpd_register_uri_handler(server, &uri_get);
    }
    /* If server failed to start, handle will be NULL */
    return server;
}

void stop_webserver(httpd_handle_t server)
{
    if (server) {
        httpd_stop(server);
    }
}

/*
	Start up.
*/
void app_main(void)
{
	TimerHandle_t frame_timer;
	// Init stuff
	init_port();
	init_buffers();
	
	// Default filters, a HSL rotate with a gaussian sweep
	const char foo[] = "8E 359 * 3E 359 % 5 * + O 1O 0.3O";
	memcpy(&filter_list[0], &foo, strlen(foo));
	filter_list[0].mod = &mod_hsl;

	const char bar[] = "1EO 1O 3E 10 / 49 % O 0.5O 0.05O";
	memcpy(&filter_list[1], &bar, strlen(bar));
	filter_list[1].mod = &mod_gaussian;	
	
	// Init NV memory
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    
	// Init WiFi
	// WiFi has to be pinned to core 1 in the build config or the string will glitch out
    wifi_init_sta();		
	start_webserver();

	// Set up the frame timer
	uint frame_timer_id = 1;
	frame_timer = xTimerCreate("Frame", pdMS_TO_TICKS(FRAME_TIME), pdTRUE, (void *)frame_timer_id, &frame_callback);
	// Start it
	if( xTimerStart(frame_timer, 10 ) != pdPASS ) {
		ESP_LOGI(TAG, "Frame timer start error");
    }

	// Do other stuff or just spin and wait for interrupts
	while(1) {
		vTaskDelay(15);
	}
}
